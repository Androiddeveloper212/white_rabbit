package com.myrxprofile.androidcodingtest.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.myrxprofile.androidcodingtest.MainActivity
import com.myrxprofile.androidcodingtest.R
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import com.myrxprofile.androidcodingtest.view.details.EmployeeDetailsPage

class EmployeeListAdapter(val mainActivity: MainActivity,var employeeItemList: ArrayList<EmployeeListModelItem>) : RecyclerView.Adapter<EmployeeListAdapter.EmployeeVH>() {



    public class EmployeeVH(item: View):RecyclerView.ViewHolder(item){
        var name :TextView = item.findViewById(R.id.name)
        var companyName :TextView = item.findViewById(R.id.companyName)
        var profileImage :ImageView = item.findViewById(R.id.profileImage)

        fun bindViewContent(item: EmployeeListModelItem, mainActivity: MainActivity){
            name.setText(item.name)
            companyName.setText(item.company?.name)
            Glide.with(mainActivity)
                .load(item.profile_image).apply(RequestOptions().circleCrop()).error(R.mipmap.ic_launcher)
                .into(profileImage)

           profileImage.setOnClickListener {
                val intent = Intent(mainActivity,EmployeeDetailsPage::class.java)
                intent.putExtra("id",item.id)
                mainActivity.startActivity(intent)
            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeVH {
        return EmployeeVH(LayoutInflater.from(parent.context).inflate(R.layout.child_employee,parent,false))
    }

    override fun onBindViewHolder(holder: EmployeeVH, position: Int) {
         holder.bindViewContent(employeeItemList[position],mainActivity)

    }

    override fun getItemCount(): Int {
        return employeeItemList.size
    }

    fun updateDateContent(list : ArrayList<EmployeeListModelItem>){
        this.employeeItemList = list
        notifyDataSetChanged()
    }

}
