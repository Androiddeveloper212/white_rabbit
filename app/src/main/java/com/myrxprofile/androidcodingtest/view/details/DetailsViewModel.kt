package com.myrxprofile.androidcodingtest.view.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.myrxprofile.androidcodingtest.EmployeeApp
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import com.myrxprofile.androidcodingtest.room.Employee
import com.myrxprofile.androidcodingtest.room.EmployeeDatabase
import com.myrxprofile.androidcodingtest.room.EmployeeServiceDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailsViewModel:ViewModel() {
    val mutableEmployeeLiveData = MutableLiveData<ArrayList<EmployeeListModelItem>>()
    fun getEmployeeDetails(id :Int){
        CoroutineScope(Dispatchers.IO).launch {
            val employeeServiceDao = EmployeeDatabase.getDatabase(EmployeeApp.instance).employeeDao()
            val list =  employeeServiceDao.getEmployeeDetailsById(id)
            customizeData(list,employeeServiceDao)
        }
    }



    private fun customizeData(employeeList: List<Employee>, employeeServiceDao: EmployeeServiceDao) {
        val employeeListAfterSearch = ArrayList<EmployeeListModelItem>()
        for (data in employeeList) {
            val employeeId = data.id
            val serverAddress = employeeId?.let { employeeServiceDao.getAddressByEmployeeId(it) }
            val serverCompany = employeeServiceDao.getCompanyByEmployeeId(employeeId)
            val serverGeo = employeeServiceDao.getGeoByEmployeeId(employeeId)

            val geo = com.myrxprofile.androidcodingtest.model.Geo(serverGeo.lat, serverGeo.lng)
            var company :com.myrxprofile.androidcodingtest.model.Company?=null
            if (serverCompany!=null){
                company = com.myrxprofile.androidcodingtest.model.Company(
                    serverCompany.bs,
                    serverCompany.catchPhrase,
                    serverCompany.name
                )
            }

            val address = com.myrxprofile.androidcodingtest.model.Address(
                serverAddress?.city,
                geo,
                serverAddress?.street,
                serverAddress?.suite,
                serverAddress?.zipcode
            )

            val employee = EmployeeListModelItem(
                address,
                company,
                data.email,
                data.id,
                data.name,
                data.phone,
                data.profile_image,
                data.user_name,
                data.website
            )
            employeeListAfterSearch.add(employee)
        }

        mutableEmployeeLiveData.postValue(employeeListAfterSearch)
    }
}