package com.myrxprofile.androidcodingtest.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.myrxprofile.androidcodingtest.MainActivity
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Dispatcher

class EmployeeViewModel:ViewModel() {
    var employeeRepository:EmployeeRepository? = null
    var employeeList = MutableLiveData<ArrayList<EmployeeListModelItem>>()
    var error = MutableLiveData<String>()

    init {
            employeeRepository = EmployeeRepository.getInstance()
            employeeList = employeeRepository!!.getEmployeeLiveData()
        error = employeeRepository!!.getError_()
    }

    fun getEmployeeList() {
        employeeRepository?.getEmployeeList()
    }

    fun getSearchedEmployees(query: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            employeeRepository?.searchEmployee(query)
        }
    }

}