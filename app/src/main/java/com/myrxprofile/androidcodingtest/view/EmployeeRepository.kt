package com.myrxprofile.androidcodingtest.view

import androidx.lifecycle.MutableLiveData
import com.myrxprofile.androidcodingtest.EmployeeApp
import com.myrxprofile.androidcodingtest.api.RestServiceBuilder
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import com.myrxprofile.androidcodingtest.room.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class EmployeeRepository {

    val mutableEmployeeLiveData = MutableLiveData<ArrayList<EmployeeListModelItem>>()
    val error = MutableLiveData<String>()

    fun getEmployeeList() {

        CoroutineScope(Dispatchers.IO).launch {
            val employeeServiceDao = EmployeeDatabase.getDatabase(EmployeeApp.instance).employeeDao()
            val list = employeeServiceDao.getAllEmployeeList()
            if (list.size==0){
                callDataFromNetWork()
            }else{
                customizeData(list,employeeServiceDao)
            }
        }




    }

    private fun callDataFromNetWork() {
        RestServiceBuilder.apiService?.getEmployeeList()
            ?.enqueue(object : Callback<ArrayList<EmployeeListModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<EmployeeListModelItem>>,
                    response: Response<ArrayList<EmployeeListModelItem>>
                ) {
                    if (response.isSuccessful) {
                        CoroutineScope(Dispatchers.IO).launch {
                            insertValuesToServer(response.body())
                        }
                        mutableEmployeeLiveData.postValue(response.body())
                    } else {
                        error.postValue(response.errorBody()?.string())
                    }
                }

                override fun onFailure(call: Call<ArrayList<EmployeeListModelItem>>, t: Throwable) {
                    error.postValue(t.localizedMessage)
                }

            })
    }

    private fun insertValuesToServer(
        employee_: List<EmployeeListModelItem>?
    ) {
        val employeeServiceDao = EmployeeDatabase.getDatabase(EmployeeApp.instance).employeeDao()
        if (employee_ != null) {
            for (data in employee_) {
                val employee = Employee(
                    data.id,
                    data.name,
                    data.username,
                    data.email,
                    data.profile_image,
                    data.phone,
                    data.website
                )
                employeeServiceDao.insertEmployeeDetails(employee)
                val address = Address(
                    generateRandomIdNumbers(),
                    data.address.street,
                    data.address.suite,
                    data.address.city,
                    data.address.zipcode,
                    data.id
                )
                employeeServiceDao.insertAddress(address)
                if (data.company!=null){
                    val company = Company(
                        generateRandomIdNumbers(),
                        data.company.name,
                        data.company.catchPhrase,
                        data.company.bs,
                        data.id
                    )
                    employeeServiceDao.insertCompany(company)
                }

                val geo = Geo(
                    generateRandomIdNumbers(),
                    data.address.geo.lat,
                    data.address.geo.lng,
                    data.id
                )
                employeeServiceDao.insertGeo(geo)
            }
        }
    }

    fun generateRandomIdNumbers(): Int {
        val min = 20
        val max = 80
        return Random().nextInt(max - min + 1) + min
    }

    fun getEmployeeLiveData(): MutableLiveData<ArrayList<EmployeeListModelItem>> {
        return mutableEmployeeLiveData
    }

    fun searchEmployee(query: String?) {

        val employeeServiceDao = EmployeeDatabase.getDatabase(EmployeeApp.instance).employeeDao()

        val employeeList = employeeServiceDao.getSearchedEmployee(query!!)

        customizeData(employeeList,employeeServiceDao)



    }

    private fun customizeData(employeeList: List<Employee>, employeeServiceDao: EmployeeServiceDao) {
        val employeeListAfterSearch = ArrayList<EmployeeListModelItem>()
        for (data in employeeList) {
            val employeeId = data.id
            val serverAddress = employeeId?.let { employeeServiceDao.getAddressByEmployeeId(it) }
            val serverCompany = employeeServiceDao.getCompanyByEmployeeId(employeeId)
            val serverGeo = employeeServiceDao.getGeoByEmployeeId(employeeId)

            val geo = com.myrxprofile.androidcodingtest.model.Geo(serverGeo.lat, serverGeo.lng)
            var company :com.myrxprofile.androidcodingtest.model.Company?=null
            if (serverCompany!=null){
                company = com.myrxprofile.androidcodingtest.model.Company(
                    serverCompany.bs,
                    serverCompany.catchPhrase,
                    serverCompany.name
                )
            }

            val address = com.myrxprofile.androidcodingtest.model.Address(
                serverAddress?.city,
                geo,
                serverAddress?.street,
                serverAddress?.suite,
                serverAddress?.zipcode
            )

            val employee = EmployeeListModelItem(
                address,
                company,
                data.email,
                data.id,
                data.name,
                data.phone,
                data.profile_image,
                data.user_name,
                data.website
            )
            employeeListAfterSearch.add(employee)
        }

        mutableEmployeeLiveData.postValue(employeeListAfterSearch)
    }

    fun getError_(): MutableLiveData<String> {
        return error
    }

    companion object {
        private val employeeRepository: EmployeeRepository? = null
        fun getInstance(): EmployeeRepository {
            if (employeeRepository == null) {
                return EmployeeRepository()
            }
            return employeeRepository
        }
    }
}