package com.myrxprofile.androidcodingtest.view.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.myrxprofile.androidcodingtest.R
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import java.lang.StringBuilder

class EmployeeDetailsPage : AppCompatActivity() {
    var detailsViewModel:DetailsViewModel?= null
    var profile_image : ImageView? = null
    var details : TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details_page)
        profile_image = findViewById(R.id.profile_image)
        details = findViewById(R.id.details)
        detailsViewModel = ViewModelProvider(this)[DetailsViewModel::class.java]
        if (intent.hasExtra("id")){
            val id = intent.getIntExtra("id",-1)
            detailsViewModel?.getEmployeeDetails(id)
            detailsViewModel?.mutableEmployeeLiveData?.observe(this, Observer {
                populateData(it)
            })
        }
    }

    private fun populateData(it: ArrayList<EmployeeListModelItem>) {
        if (it.size>0){
            val detailsEmployee = it[0]
            val detailsBuilder = StringBuilder()
            detailsBuilder.append("Name :${detailsEmployee.name}")
            detailsBuilder.append("\n")
            detailsBuilder.append("Username : ${detailsEmployee.username}")
            detailsBuilder.append("\n")
            detailsBuilder.append("Email :${detailsEmployee.email}")
            detailsBuilder.append("\n")
            if (detailsEmployee.address!=null){
                detailsBuilder.append("Address : ${detailsEmployee.address.suite}, ${detailsEmployee.address.street} \n" +
                        "${detailsEmployee.address.city}, Pin : ${detailsEmployee.address.zipcode}")
                detailsBuilder.append("\n")
            }

            detailsBuilder.append("Phone :${detailsEmployee.phone}")
            detailsBuilder.append("\n")
            detailsBuilder.append("Website : ${detailsEmployee.website}")
            detailsBuilder.append("\n")
            if (detailsEmployee.company!=null){
                detailsBuilder.append("Company details :${detailsEmployee.company?.name} ,${detailsEmployee.company?.catchPhrase}, ${detailsEmployee.company?.bs}")
            }

            profile_image?.let { it1 ->
                Glide.with(this)
                    .load(detailsEmployee.profile_image).apply(RequestOptions().circleCrop()).error(R.mipmap.ic_launcher)
                    .into(it1)
            }

            details?.setText(detailsBuilder)
        }




    }


}