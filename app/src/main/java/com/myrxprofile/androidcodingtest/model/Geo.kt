package com.myrxprofile.androidcodingtest.model

data class Geo(
    val lat: String?,
    val lng: String?
)