package com.myrxprofile.androidcodingtest

import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.myrxprofile.androidcodingtest.adapter.EmployeeListAdapter
import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import com.myrxprofile.androidcodingtest.view.EmployeeViewModel

class MainActivity : AppCompatActivity() {
    var employeeItemList = ArrayList<EmployeeListModelItem>()
    var employeeViewModel: EmployeeViewModel? = null
    private val TAG = "MainActivity"
    var employeeList: RecyclerView? = null
    var searchEmployee: SearchView? = null
    var employeeListAdapter:EmployeeListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        employeeList = findViewById(R.id.employeeList)
        searchEmployee = findViewById(R.id.searchEmployee)
        setRecyclerView()
        employeeViewModel = ViewModelProvider(this)[EmployeeViewModel::class.java]
        employeeViewModel?.getEmployeeList()
        employeeViewModel?.employeeList?.observe(this, Observer {
            Log.d(TAG, "onCreate: list size ${it.size}")
            setEmployeeList(it)
        })
        searchEmployee?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query?.length!=0){
                    employeeViewModel?.getSearchedEmployees(query)
                }else{
                    employeeViewModel?.getEmployeeList()
                }

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // Need to type full name to get output
                //Like Ervin Howell
                if (newText?.length!=0){
                    employeeViewModel?.getSearchedEmployees(newText)
                }else{
                    employeeViewModel?.getEmployeeList()
                }

                return true
            }

        })

        employeeViewModel?.error?.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        employeeViewModel?.getEmployeeList()
    }

    private fun setEmployeeList(it: ArrayList<EmployeeListModelItem>?) {
        if (it != null) {
            employeeItemList = it
            if (employeeListAdapter!=null){
                employeeListAdapter?.updateDateContent(employeeItemList)
            }
        }

    }

    private fun setRecyclerView() {
        if (employeeListAdapter==null){
            employeeListAdapter = EmployeeListAdapter(this,employeeItemList)
        }
        employeeList?.layoutManager = LinearLayoutManager(this)
        employeeList?.adapter = employeeListAdapter
    }

    override fun onResume() {
        super.onResume()

    }
}