package com.myrxprofile.androidcodingtest.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "geo")
data class Geo(
    @PrimaryKey() var id: Int,
    @ColumnInfo(name = "lat") var lat: String?,
    @ColumnInfo(name = "lng") var lng: String?,
    @ColumnInfo(name = "employee_id") var employee_id: Int?
)
