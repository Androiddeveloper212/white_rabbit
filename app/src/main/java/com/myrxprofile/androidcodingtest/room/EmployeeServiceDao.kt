package com.myrxprofile.androidcodingtest.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import retrofit2.http.GET

@Dao
interface EmployeeServiceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployeeDetails(employee: Employee)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddress(address: Address)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompany(company: Company)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGeo(geo: Geo)

    @Query("SELECT * FROM employee WHERE name LIKE :query OR email LIKE :query")
    fun getSearchedEmployee(query:String):List<Employee>

    @Query("SELECT * FROM address WHERE employee_id LIKE :id")
    fun getAddressByEmployeeId(id:Int?):Address

    @Query("SELECT * FROM company WHERE employee_id LIKE :id")
    fun getCompanyByEmployeeId(id:Int?):Company

    @Query("SELECT * FROM geo WHERE employee_id LIKE :id")
    fun getGeoByEmployeeId(id:Int?):Geo

    @Query("SELECT * FROM employee ")
    fun getAllEmployeeList():List<Employee>

    @Query("SELECT * FROM employee WHERE id LIKE :id")
    fun getEmployeeDetailsById(id: Int):List<Employee>

}