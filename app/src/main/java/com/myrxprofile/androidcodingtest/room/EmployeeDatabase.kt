package com.myrxprofile.androidcodingtest.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Geo::class, Address::class,Company::class,Employee::class],version = 3,exportSchema = false)
abstract class EmployeeDatabase :RoomDatabase(){
    abstract fun employeeDao(): EmployeeServiceDao
    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: EmployeeDatabase? = null

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Since we didn't alter the table, there's nothing else to do here.
                database.execSQL("CREATE TABLE IF NOT EXISTS `medicine_pending` (`id` INTEGER NOT NULL, `completion_date` TEXT,  `completion_status` INTEGER, pending_status INTEGER DEFAULT 0, PRIMARY KEY(`id`))");

            }
        }


        fun getDatabase(context: Context): EmployeeDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    EmployeeDatabase::class.java,
                    "word_database"
                ).addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}