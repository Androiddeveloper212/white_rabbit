package com.myrxprofile.androidcodingtest.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employee")
data class Employee(
    @PrimaryKey() var id: Int?,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "user_name") var user_name: String?,
    @ColumnInfo(name = "email") var email: String?,
    @ColumnInfo(name = "profile_image") var profile_image: String?,
    @ColumnInfo(name = "phone") var phone: String?,
    @ColumnInfo(name = "website") var website: String?,
)
