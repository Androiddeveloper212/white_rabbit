package com.myrxprofile.androidcodingtest.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address")
data class Address(
    @PrimaryKey() var id: Int,
    @ColumnInfo(name = "street") var street: String?,
    @ColumnInfo(name = "suite") var suite: String?,
    @ColumnInfo(name = "city") var city: String?,
    @ColumnInfo(name = "zipcode") var zipcode: String?,
    @ColumnInfo(name = "employee_id") var employee_id: Int?
)
