package com.myrxprofile.androidcodingtest.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "company")
data class Company(
    @PrimaryKey() var id: Int,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "catchPhrase") var catchPhrase: String?,
    @ColumnInfo(name = "bs") var bs: String?,
    @ColumnInfo(name = "employee_id") var employee_id: Int?
)
