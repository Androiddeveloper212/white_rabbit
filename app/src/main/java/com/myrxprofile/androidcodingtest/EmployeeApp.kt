package com.myrxprofile.androidcodingtest

import android.app.Application

class EmployeeApp:Application() {
    companion object {
        lateinit var instance: EmployeeApp
    }

    override fun onCreate() {
        super.onCreate()

//        TestFairy.begin(this, BaseUrl.getTestFairyUrl(Constants.BASE_URL_TYPE))
        instance = this

    }
}