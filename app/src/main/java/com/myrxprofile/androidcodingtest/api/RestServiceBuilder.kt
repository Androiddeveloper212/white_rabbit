package com.myrxprofile.androidcodingtest.api

object RestServiceBuilder {
    private var service: RestService? = null
    val apiService: RestService?
        get() {
            service =
                ServiceGenerator.createService(RestService::class.java)
            return service
        }
}