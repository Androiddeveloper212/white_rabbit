package com.myrxprofile.androidcodingtest.api



import com.myrxprofile.androidcodingtest.model.EmployeeListModelItem
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface RestService {

//    ********GET*********


    @GET("v2/5d565297300000680030a986")
    fun getEmployeeList(): Call<ArrayList<EmployeeListModelItem>>

}